<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://b4kodi.xyz/ra-download-control
 * @since             1.0.0
 * @package           Ra_Download_Control
 *
 * @wordpress-plugin
 * Plugin Name:       RA-Download-Control
 * Plugin URI:        http://b4kodi.xyz/ra-download-control
 * Description:       This plugin allows the control of file downloads by allowing you to store them outside of the websites root folder, therefore removing the possibility of direct access. Access is given via user being logged in and has certain role or Membership level via use of 3rd party plugins.
 * Version:           1.0.0
 * Author:            Ron Appleton
 * Author URI:        http://b4kodi.xyz/ra-download-control
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       ra-download-control
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-ra-download-control-activator.php
 */
function activate_ra_download_control() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ra-download-control-activator.php';
	Ra_Download_Control_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-ra-download-control-deactivator.php
 */
function deactivate_ra_download_control() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ra-download-control-deactivator.php';
	Ra_Download_Control_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_ra_download_control' );
register_deactivation_hook( __FILE__, 'deactivate_ra_download_control' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-ra-download-control.php';
/**
 * The download class.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class.chip_download.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_ra_download_control() {

	$plugin = new Ra_Download_Control();
	$plugin->run();

}

/**
 * Now we add the options for our plugin
 */

add_action('parse_request', 'ra_download_control_url_handler');

function ra_download_control_url_handler()
{
    /*
     * THIS WORKS FOR CHECKING MEMBERSHIP2 Sub
     * ms_api();
        if(ms_has_membership()){
        print 'This is a MS member!!!!';
        exit;
        };
     *
     */

    if (strpos($_SERVER['REQUEST_URI'], 'ra_download') !== false) {

        if (!is_user_logged_in()) {
            auth_redirect();
        }
        //Ok so user is logged in, now to reconstruct request and return it.
        //First we need to seperate the parts.
        $options = get_option('ra_download_control_settings');
        $path_parts = explode('/', $_SERVER['REQUEST_URI']);
        $clean_parts = cleanURL($path_parts);
        $file_name = $clean_parts[count($clean_parts) - 1];
        unset($clean_parts[count($clean_parts) - 1]);
        $clean_parts = array_values($clean_parts);
        $real_path = '';
        for ($i = 0; $i < count($clean_parts); $i++) {
            $real_path .= $clean_parts[$i] . '/';
        }
        $our_path = '/home/b4kodi/' . $options['ra_download_control_text_field_0'] . '/' . $real_path;

        $args = array(
            'download_path' => $our_path,
            'file' => $file_name,
            'extension_check' => TRUE,
            'referrer_check' => FALSE,
            'referrer' => NULL,
        );
        $download = new chip_download($args);
        $download_hook = $download->get_download_hook();
        //$download->chip_print($download_hook);
        //exit;
        if ($download_hook['download'] == TRUE) {

            /* You can write your logic before proceeding to download */

            /* Let's download file */
            $download->get_download();
        }
    }
}

    function cleanURL($url)
    {
        //Returns path minus any ../
        if (($key = array_search('../', $url)) !== false) {
            unset($url[$key]);
        }
        if (($key = array_search('b4kodi.xyz', $url)) !== false) {
            unset($url[$key]);
        }
        if (($key = array_search('ra_download', $url)) !== false) {
            unset($url[$key]);
        }
        if (($key = array_search('', $url)) !== false) {
            unset($url[$key]);
        }
        return array_values($url);
    }

    add_action('admin_menu', 'ra_download_control_add_admin_menu');
    add_action('admin_init', 'ra_download_control_settings_init');


    function ra_download_control_add_admin_menu()
    {

        add_options_page('RA Download Control', 'RA Download Control', 'manage_options', 'ra_download_control', 'ra_download_control_options_page');

    }


    function ra_download_control_settings_init()
    {

        register_setting('pluginPage', 'ra_download_control_settings');

        add_settings_section(
            'ra_download_control_pluginPage_section',
            __('General Settings', 'wordpress'),
            'ra_download_control_settings_section_callback',
            'pluginPage'
        );

        add_settings_field(
            'ra_download_control_text_field_0',
            __('File Folder Name', 'wordpress'),
            'ra_download_control_text_field_0_render',
            'pluginPage',
            'ra_download_control_pluginPage_section'
        );


    }


    function ra_download_control_text_field_0_render()
    {

        $options = get_option('ra_download_control_settings');
        ?>
        <input type='text' name='ra_download_control_settings[ra_download_control_text_field_0]'
               value='<?php echo $options['ra_download_control_text_field_0']; ?>'>
        <p>
            <small>This folder <b>must</b> be on the same level as public_html or www!</small>
        </p>
    <?php

    }


    function ra_download_control_settings_section_callback()
    {

        echo __('Restrict Access To File Downloads', 'wordpress');

    }


    function ra_download_control_options_page()
    {

        ?>
        <form action='options.php' method='post'>

            <h1>RA Download Control</h1>

            <?php
            settings_fields('pluginPage');
            do_settings_sections('pluginPage');
            submit_button();
            ?>

        </form>
    <?php

    }

/**
 * End of options.
 */
run_ra_download_control();