=== Plugin Name ===
Contributors: dodgerid
Donate link: http://b4kodi.xyz/ra-download-control
Tags: comments, spam
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Wordpress plugin to allow controlled downloads of files from outside of your web root.

== Description ==

Wordpress plugin to allow controlled downloads of files from outside of your web root.

This plugin ensures that the user is logged in before allowing download, but by controlling your downloads from outside of your web root your files can never be directly linked too.

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload `ra-download-control.php` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress


== Screenshots ==

== Changelog ==

= 1.0 =
* Initial Version