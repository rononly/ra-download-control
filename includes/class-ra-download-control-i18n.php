<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://b4kodi.xyz/ra-download-control
 * @since      1.0.0
 *
 * @package    Ra_Download_Control
 * @subpackage Ra_Download_Control/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Ra_Download_Control
 * @subpackage Ra_Download_Control/includes
 * @author     Ron Appleton <ron.appleton@gmail.com>
 */
class Ra_Download_Control_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'ra-download-control',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
