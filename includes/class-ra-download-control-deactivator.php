<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://b4kodi.xyz/ra-download-control
 * @since      1.0.0
 *
 * @package    Ra_Download_Control
 * @subpackage Ra_Download_Control/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Ra_Download_Control
 * @subpackage Ra_Download_Control/includes
 * @author     Ron Appleton <ron.appleton@gmail.com>
 */
class Ra_Download_Control_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
