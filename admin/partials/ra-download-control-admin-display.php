<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://b4kodi.xyz/ra-download-control
 * @since      1.0.0
 *
 * @package    Ra_Download_Control
 * @subpackage Ra_Download_Control/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
